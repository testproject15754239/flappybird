﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FlappyBird0._1._0
{
    public partial class Form1 : Form
    {
        Player bird;

        TheWall wall1;
        TheWall wall2;

        Apple apple;

        float gravity;
        int s;

        public Form1()
        {
            InitializeComponent();
            timer1.Interval = 40;
            timer1.Tick += new EventHandler(update);
            Init();
            Invalidate();

            button2.Visible = false;
            label1.Visible = false;
        }

        public void Init()
        {
            s = 0;
            bird = new Player(200, 200);
            wall1 = new TheWall(600, -100, true);
            wall2 = new TheWall(600, 300);
            apple = new Apple(590, 200);

            gravity = 0;

            this.Text = "Flappy Bird Score: " + s;
            timer1.Start();
        }

        private void update(object sender, EventArgs e)
        {
            if (bird.y > 600){
                bird.isAlive = false;
                timer1.Stop();
                label1.Visible = true;
                button2.Visible = true;
                //Init();
            }

            if (Collide(bird, wall1) || Collide(bird, wall2)){
                bird.isAlive = false;
                timer1.Stop();
                label1.Visible = true;
                button2.Visible = true;
                //Init();
            }

            if (CollideApple(bird, apple))
            {
                EatApple();
            }

            if (bird.gravityValue != 0.1f)
                bird.gravityValue += 0.115f;
            bird.y += bird.gravityValue;

            if (bird.isAlive)
            {
                MoveWalls();
            }

            Invalidate();
        }

        private bool CollideApple(Player bird, Apple apple)
        {
            PointF delta = new PointF();
            delta.X = (bird.x + bird.size / 2) - (apple.x + apple.size / 2);
            delta.Y = (bird.y + bird.size / 2) - (apple.y + apple.size / 2);
            if (Math.Abs(delta.X) <= bird.size / 2 + apple.size / 2)
            {
                if (Math.Abs(delta.Y) <= bird.size / 2 + apple.size / 2)
                {
                    return true;
                }
            }
            return false;
        }

        private bool Collide(Player bird, TheWall wall1)
        {
            PointF delta = new PointF();
            delta.X = (bird.x + bird.size / 2) - (wall1.x + wall1.sizeX / 2);
            delta.Y = (bird.y + bird.size / 2) - (wall1.y + wall1.sizeY / 2);
            if (Math.Abs(delta.X) <= bird.size / 2 + wall1.sizeX / 2)
            {
                if (Math.Abs(delta.Y) <= bird.size / 2 + wall1.sizeY / 2)
                {
                    return true;
                }
            }
            return false;
        }

        private void EatApple()
        {
            apple.size = 0;
        }

        private void CreateNewWall()
        {
            if (wall1.x < bird.x - 250)
            {
                Random r = new Random();
                int y1;
                y1 = r.Next(-200, 000);
                wall1 = new TheWall(700, y1, true);
                wall2 = new TheWall(700, y1+400);
                apple = new Apple(690, y1 + 300);
                bird.score++;
                s++;
                this.Text = "Flappy Bird Score: " + s;
            }
        }

        private void MoveWalls(){
            for (int i = 0; i <= bird.score; i++)
            {
                wall1.x -= 2 + i / 5;
                wall2.x -= 2 + i / 5;
                apple.x -= 2 + i / 5;
            }
            CreateNewWall();
        }

        private void OnPaint(object sender, PaintEventArgs e)
        {
            Graphics graphics = e.Graphics;

            graphics.DrawImage(bird.birdImg, bird.x, bird.y, bird.size, bird.size);

            graphics.DrawImage(wall1.wallImg, wall1.x, wall1.y, wall1.sizeX, wall1.sizeY);

            graphics.DrawImage(wall2.wallImg, wall2.x, wall2.y, wall2.sizeX, wall2.sizeY);

            graphics.DrawImage(apple.appleImg, apple.x, apple.y, apple.size, apple.size);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (bird.isAlive)
            {
                gravity = 0;
                bird.gravityValue = -1.9f;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Init();
            label1.Visible = false;
            button2.Visible = false;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
