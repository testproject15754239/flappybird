﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace FlappyBird0._1._0
{
    class Player
    {
        public float x;
        public float y;

        public int size;
        public int score;
        public int s;

        public Image birdImg;

        public float gravityValue;

        public bool isAlive;

        public Player(int x, int y)
        {
            birdImg = new Bitmap("C:\\Users\\Анна\\Desktop\\F\\flappybird\\FlappyBird0.1.0\\flappy.png");
            this.x = x;
            this.y = y;
            size = 35;
            gravityValue = 5;
            isAlive = true;
            score = 2;
            s = 0;
    }
    }
}
