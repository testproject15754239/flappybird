﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace FlappyBird0._1._0
{
    class TheWall
    {
        public int x;
        public int y;

        public int sizeX;
        public int sizeY;

        public Image wallImg;
        public TheWall(int x, int y, bool isRotatedImage= false)
        {
            wallImg = new Bitmap("C:\\Users\\Анна\\Desktop\\F\\flappybird\\FlappyBird0.1.0\\tube.png");
            this.x = x;
            this.y = y;
            sizeX = 35;
            sizeY = 240;
            if (isRotatedImage)
                wallImg.RotateFlip(RotateFlipType.Rotate180FlipX);
        }
    }
}
